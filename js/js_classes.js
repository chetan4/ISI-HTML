//Modal Window Constructor(Class)
function ModalObj(clickElem, hideElem, overLay) {
  this.clickElem = clickElem; //Clickevent element
  this.hideElem = hideElem; //element that needs to be hidden
  this.overLay = overLay;
  this.closeModal = function () {
    var _this = this;
    $(_this.clickElem).click(function (i) {
      i.preventDefault();
      if ($(hideElem).length > 0) {
        $(_this.hideElem).fadeOut('fast').remove();
        $(_this.overLay).fadeOut('fast').remove();
      } else {
        console.log('something went wrong');
      }
    });
  };
}


//Tabs Constructor(Class)
function Tabs(activeClass, tabContentElm, tabElm) {
  this.activeClass = activeClass;
  this.tabContentElm = tabContentElm;
  this.tabElm = tabElm;
  this.tabify = function () {
    $(this.tabElm).removeClass(this.activeClass);
    $(this.tabElm + ':first-child').addClass(this.activeClass);
    $(this.tabContentElm).hide();
    $(this.tabContentElm + ':first').show();
    var _this = this;
    $(_this.tabElm).click(function () {
      $(_this.tabElm).removeClass(_this.activeClass);
      $(this).addClass(_this.activeClass);
      var activeTab = $(this).find('a').attr('href');
      $(_this.tabContentElm).hide();
      $(activeTab).show();
      return false;
    });
  };
}